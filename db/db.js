'use strict';

const tradeSchema = require('../db/schema/trade.json');
const elasticsearch = require('elasticsearch');

module.exports = DB;
function DB(args) {

    if(args.length == 0) return;

    args.forEach((val, index) => {
        console.log(`${index}: ${val}`);
    });

    switch (args[0])
    {
        case "install":
        {
            this.install();
        }break;
        case "uninstall":
        {
            this.uninstall();
        }break;

        default:
            console.log(`The command ${args[0]} not supported`);
    }

    this.connect = function () {
        return elastic.ping({
            requestTimeout: 3000
        }).then(function () {
            return this;
        }, function () {

        });
    };

    this.install = function() {

        let elastic = new elasticsearch.Client({
            host: 'localhost:9200',
            log: 'trace',
        });

        elastic.ping({
            // ping usually has a 3000ms timeout
            requestTimeout: 3000
        }, function (error) {
            if (error) {
                console.trace('elasticsearch cluster is down!');
            } else {
                console.log('All is well');
                elastic.indices.create({index:'trade'}, function (error, response) {
                    console.log(`Error: ${error}:${response}`);
                });
            }
        });
    }

    this.uninstall = function() {

        let elastic = new elasticsearch.Client({
            host: 'localhost:9200',
            log: 'trace',
        });

        elastic.ping({
            // ping usually has a 3000ms timeout
            requestTimeout: 3000
        }, function (error) {
            if (error) {
                console.trace('elasticsearch cluster is down!');
            } else {
                console.log('All is well');
                elastic.indices.delete({index:'trade'}, function (error, response) {
                    console.log(`Error: ${error}:${response}`);
                });
            }
        });
    }

}

const db = new DB(process.argv.slice(2));