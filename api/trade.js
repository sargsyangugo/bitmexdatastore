const express = require('express');
const config = require('config');
const sql = require('mssql');
const router = express.Router();
const _ = require('lodash');
const moment = require('moment');
const connectionString = config.get('mssql.connectionString');

sql.on('error', err => {
    console.log(`SQL error: ${err}`);
})

const query = `SELECT top 10 CAST([timestamp] AS DATE) as timestamp, AVG([price]) as price
FROM [bitmex].[dbo].[trade]
GROUP BY CAST([timestamp] AS DATE) order by timestamp`


router.get('/get', function(req, res, next) {

    const pool = new sql.ConnectionPool(connectionString, err => {
        pool.request()
            .query(query, (err, result) => {
                if (result.recordset) {
                    let data = _.map(result.recordset, function (record) {
                        return [moment(record.timestamp).startOf('day').valueOf(), record.price]
                    })
                    res.send(data);
                }
            })
    })
});

module.exports = router;

