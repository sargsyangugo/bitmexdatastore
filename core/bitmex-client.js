'use strict';

const BitMEXClient = require('../core/bitmex/index');
const elasticsearch = require('elasticsearch');
const sql = require('mssql');
const config = require('config');

const sqlConnectionString = config.get('mssql.connectionString');
const elasticConnectionString = config.get('elastic.connectionString');
const elasticAuth = config.get('elastic.auth');

sql.on('error', err => {
    console.log(`SQL error: ${err}`);
})

function BitmexClient() {

    let bitmex = new BitMEXClient();
    let elastic = new elasticsearch.Client({
        host: elasticConnectionString,
        //auth: elasticAuth,
        //log: 'trace',
    });

    const insertQuery = `INSERT INTO [trade]
           ([trdMatchID]
           ,[timestamp]
           ,[tickDirection]
           ,[symbol]
           ,[size]
           ,[side]
           ,[price]
           ,[homeNotional]
           ,[grossValue]
           ,[foreignNotional])
     VALUES
           (@trdMatchID
           ,@timestamp
           ,@tickDirection
           ,@symbol
           ,@size
           ,@side
           ,@price
           ,@homeNotional
           ,@grossValue
           ,@foreignNotional)`;

    this.start = function() {

        bitmex.on('error', console.error);
        bitmex.on('open', () => console.log('Connection opened.'));
        bitmex.on('close', () => console.log('Connection closed.'));
        bitmex.on('initialize', () => console.log('Client initialized, data is flowing.'));

        bitmex.addStream('XBTUSD', 'trade', function(data, symbol, tableName) {
            //console.log(`${tableName}:${symbol} Data:\n${JSON.stringify(data)}`);
            //console.log(`${JSON.stringify(data)}`);

            const pool = new sql.ConnectionPool(sqlConnectionString, err => {
                if(err) {
                    console.log(`SQL error: ${err}`);
                }else {

                    var trades = data.data;

                    console.log(`${new Date()}, Trades Count: ${trades.length}`);

                    for (var i = 0; i < trades.length; i++) {
                        var trade = trades[i];
                        let result = pool.request()
                            .input('trdMatchID', sql.UniqueIdentifier, trade.trdMatchID)
                            .input('timestamp', sql.DateTime2, trade.timestamp)
                            .input('tickDirection', sql.NChar(50), trade.tickDirection)
                            .input('symbol', sql.NChar(50), trade.symbol)
                            .input('size', sql.BigInt, trade.size)
                            .input('side', sql.NChar(50), trade.side)
                            .input('price', sql.Float, trade.price)
                            .input('homeNotional', sql.Float, trade.homeNotional)
                            .input('grossValue', sql.BigInt, trade.grossValue)
                            .input('foreignNotional', sql.Float, trade.foreignNotional)
                            .query(insertQuery, (error, result) => {
                                if (error) {
                                    console.log(`SQL INSERT ERROR: ${error}:${JSON.stringify(result)}`);
                                }
                            });

                        elastic.create({
                            //refresh:true,
                            index: 'bitmex',
                            type: 'trade',
                            id: trade.trdMatchID,
                            body: trade,
                        }, function (error, response) {
                            if(error) {
                                console.log(`ELASTIC INSERT ERROR: ${error}:${response}`);
                            }
                        });
                    }
                }
            });



        });
    }

}

module.exports = BitmexClient;